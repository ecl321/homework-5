# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp

V-----------------------------ANSWER---------------------------------------V

===>    ((λp.pz)(λq.(w(λw.(wqzp)))))

^-----------------------------ANSWER---------------------------------------^


2. λp.pq λp.qp

V-----------------------------ANSWER---------------------------------------V

===>    (λp.(pq(λp.(qp))))

^-----------------------------ANSWER---------------------------------------^







## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q
== (λs.(sz(λq(.sq))))
V-----------------------------ANSWER---------------------------------------V

s is bound to λs
z -> free
q is bound to λq

^-----------------------------ANSWER---------------------------------------^




2. (λs. s z) λq. w λw. w q z s
== (λs.sz)(λq.(wλw.(wqzs)))
V-----------------------------ANSWER---------------------------------------V

s is bound to λs
q is bound to λq
w is bound to λw
Rest -> free

^-----------------------------ANSWER---------------------------------------^






3. (λs.s) (λq.qs)
== (λs.s) (λq.qs)
V-----------------------------ANSWER---------------------------------------V

s is bound to λs
q is bound to λq
other s -> free

^-----------------------------ANSWER---------------------------------------^







4. λz. (((λs.sq) (λq.qz)) λz. (z z))
== λz.(((λs.q) (λq.qz)) λz.(zz) )
V-----------------------------ANSWER---------------------------------------V

s is bound to λs
q is bound to λq
all z is bound to λz
other z is bound λq
other -> free

^-----------------------------ANSWER---------------------------------------^







## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
V-----------------------------ANSWER---------------------------------------V

(1.)
(z)[z->λq.q q] (λs.s a)
(2.)
(λq.q q)(λs.s a)
(3.)
(q q)[q -> λs.s a]
(4.)
(λs.s a)(λs.s a)
(5.)
(s a)[s -> λs.s a]
(6.)
((λs.s a) a) [s -> a]
(7.)
(a a)

^-----------------------------ANSWER---------------------------------------^







2. (λz.z) (λz.z z) (λz.z q)

V-----------------------------ANSWER---------------------------------------V

(1.)
(z)(z -> λz.z z)(λz.zq)
(2.)
(λz.z z)(λz.z q)
(3.)
(z z)(z -> λz.z q)
(4.)
(λz.z q)(λz.z q)
(5.)
(z q)[z -> λz.z q]
(6.)
((λz.z q) q) [z -> q]
(7.)
(q q)

^-----------------------------ANSWER---------------------------------------^






3. (λs.λq.s q q) (λa.a) b
V-----------------------------ANSWER---------------------------------------V

(1.)
(λq.s qq)[s -> λa.a]b
(2.)
(λq.(λa.a)q q)b
(3.)
((λa.a) q q) [q -> b]
(4.)
(λa.a) b b
(5.)
(a)[a -> b] b
(6.)
(b b)

^-----------------------------ANSWER---------------------------------------^








4. (λs.λq.s q q) (λq.q) q
V-----------------------------ANSWER---------------------------------------V

(1.)
(λq.s q q)(s -> λq.q') q
(2.)
(λq.(λq.q') q q ) q
(3.)
((λq.q') qq)[q -> q']
(4.)
((λq.q') q' q')
(5.)
(q')[q' -> q'] q
(6.)
(q' q')

^-----------------------------ANSWER---------------------------------------^







5. ((λs.s s) (λq.q)) (λq.q)
V-----------------------------ANSWER---------------------------------------V

(1.)
(s s)[s -> λq.q] (λq.q)
(2.)
(λq.q)(λq.q)(λq.q)
(3.)
(q)[q -> λq.q](λq.q)
(4.)
(λq.q)(λq.q)
(5.)
(q)[q -> λq.q]
(6.)
(λq.q)

^-----------------------------ANSWER---------------------------------------^













## Question 4

1. Write the truth table for the or operator below.

T or T = T
T or F = T
F or T = T
F or F = F

Which translates to:
T=(λpλq.p)
F=(λpλq.q)
 and
Or = (λp.λq.p p q)





2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions.

T or T-
(λp.λq.p p q) (λpλq.p) (λpλq.p)
(λq.p p q) [p -> λpλq.p]
(λpλq.p)
(λq.(λpλq.p)(λpλq.p) (q))
(λpλq.p)
(λpλq.p)(λpλq.p)(q) [q -> λpλq.-p]



## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.






## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user).
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.
